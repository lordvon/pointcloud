For use on Linux machines.

To compile:
-Go into 'src'
-Make a dummy directory
-Go into the dummy directory and run 'cmake ..'
-Run 'make'

To run, use './pointcloud'. The point cloud data file path can be added as a command line argument.

Software Used:
-PCL
-GTK
-https://github.com/mlabbe/nativefiledialog
-OpenCV is not used in the code, but is available (included in CMakeLists.txt)

System Details:
-Ubuntu 14.04 64-bit