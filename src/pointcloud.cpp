#include <iostream>
#include <string>
 
// Point cloud library
#include <pcl/point_cloud.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/passthrough.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/cloud_viewer.h>
 
// Opencv
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

//boost
#include <boost/thread/thread.hpp>

//file dialog
#include "nfd.h"
#include "nfd_common.h"
#include "common.h"

typedef pcl::PointXYZRGB PointType;

pcl::visualization::CloudViewer viewer ("Point Cloud Visualizer");
pcl::PointCloud<PointType>::Ptr cloud(new pcl::PointCloud<PointType>);

//Simple status variables
volatile bool cloud_update = false;
volatile bool camera_update = false;
volatile bool reset_camera_update = false;
volatile int camera_view = -1;//0:x,1:y,z:2
volatile int centroid_x,centroid_y,centroid_z;

//Simple menu constants
int MENU_X = 50;
int MENU_Y = 50;
int OPTION_HEIGHT = 20;
int OPTION_WIDTH = 80;
int LOAD_RANK = 3;
int VIEW_X_RANK = 2;
int VIEW_Y_RANK = 1;
int VIEW_Z_RANK = 0;
int RESET_RANK = -1;

void displayButtons(pcl::visualization::PCLVisualizer& viewer)
{
  viewer.addText("Load File", MENU_X, MENU_Y+LOAD_RANK*OPTION_HEIGHT);
  viewer.addText("View X Normal", MENU_X, MENU_Y+VIEW_X_RANK*OPTION_HEIGHT);
  viewer.addText("View Y Normal", MENU_X, MENU_Y+VIEW_Y_RANK*OPTION_HEIGHT);
  viewer.addText("View Z Normal", MENU_X, MENU_Y+VIEW_Z_RANK*OPTION_HEIGHT);
  viewer.addText("Reset Camera", MENU_X, MENU_Y+RESET_RANK*OPTION_HEIGHT);
}

void resetCloudCamera(pcl::visualization::PCLVisualizer& viewer)
{
  viewer.resetCamera();
}

void viewNormal(pcl::visualization::PCLVisualizer& viewer)
{
  std::vector<pcl::visualization::Camera> cam;
  viewer.getCameras(cam);
  cam[0].pos[0] = (camera_view==0)?-1:0;
  cam[0].pos[1] = (camera_view==1)?-1:0;
  cam[0].pos[2] = (camera_view==2)?-1:0;
  cam[0].focal[0] = (camera_view==0)?1:0;
  cam[0].focal[1] = (camera_view==1)?1:0;
  cam[0].focal[2] = (camera_view==2)?1:0;
  cam[0].view[0] = (camera_view==2)?1:0;
  cam[0].view[1] = (camera_view==0)?1:0;
  cam[0].view[2] = (camera_view==1)?1:0;
  viewer.setCameraParameters(cam[0]);
  // cout << "Camera View: " << camera_view << endl;
  // cout << "Test: " << cam[0].focal[0] << ", " << cam[0].focal[1] << ", " << cam[0].focal[2] << endl;
  viewer.resetCamera();
}

std::string selectFile(nfdresult_t& status)
{
  std::string return_string("");

  nfdchar_t *outPath = NULL;
  nfdresult_t result = NFD_OpenDialog( NULL, NULL, &outPath );
  if(result == NFD_OKAY)
  {
    return_string = std::string(outPath);
  }
  status = result;
  return return_string;
}

bool checkMenuPress(int option_rank,int pressx,int pressy)
{
  bool inx = (pressx > MENU_X) and (pressx < (MENU_X + OPTION_WIDTH));
  int y_bottom = MENU_Y + option_rank*OPTION_HEIGHT;
  int y_top = y_bottom + OPTION_HEIGHT;
  bool iny = pressy >= y_bottom and pressy < y_top;
  bool pressed = inx and iny;
  return pressed;
}

bool loadCloud(pcl::visualization::CloudViewer& viewer, pcl::PointCloud<PointType>::Ptr cloud)
{
  // PCL_ERROR("Please choose a valid file to load.\n");
  bool show_cloud = false;
  nfdresult_t status;
  std::string cloud_file_name = selectFile(status);
  // cout<< cloud_file_name << endl;
  if(status != NFD_CANCEL)
  {
    int file_loaded = pcl::io::loadPCDFile<PointType> (cloud_file_name, *cloud);
    if(file_loaded != -1)
    {
      cloud_update = true;
    }
  }
  return show_cloud;
}

void mouseEventOccurred (const pcl::visualization::MouseEvent &event,
                         void* viewer_void)
{
  if (event.getButton () == pcl::visualization::MouseEvent::LeftButton &&
      event.getType () == pcl::visualization::MouseEvent::MouseButtonRelease)
  {
    
    if(checkMenuPress(LOAD_RANK,event.getX(),event.getY()))
    {
      if(loadCloud(*((pcl::visualization::CloudViewer*)viewer_void),cloud) == -1)
      {
        cloud_update = true;
      }
    }

    if(checkMenuPress(VIEW_X_RANK,event.getX(),event.getY()))
    {
      camera_view = 0;
      camera_update = true;
    }
    if(checkMenuPress(VIEW_Y_RANK,event.getX(),event.getY()))
    {
      camera_view = 1;
      camera_update = true;
    }
    if(checkMenuPress(VIEW_Z_RANK,event.getX(),event.getY()))
    {
      camera_view = 2;
      camera_update = true;
    }
    if(checkMenuPress(RESET_RANK,event.getX(),event.getY()))
    {
      reset_camera_update = true;
    }
    // std::cout << "Left mouse button released at position (" << event.getX () << ", " << event.getY () << ")" << std::endl;
  }
}

int main (int argc, char** argv)
{

	bool input_specified = argc > 1;
  if(input_specified)
  {
    int file_loaded = pcl::io::loadPCDFile<PointType> (argv[1], *cloud);
    if(file_loaded == -1)
    {
      loadCloud(viewer,cloud);
    }
    else
    {
      cloud_update = true;
    }
  }

  viewer.runOnVisualizationThreadOnce(displayButtons);
  viewer.registerMouseCallback(mouseEventOccurred, (void *)&viewer);

  while (!viewer.wasStopped())
  {
    if(cloud_update)
    {
      cloud_update = false;
      viewer.showCloud(cloud);
      viewer.runOnVisualizationThreadOnce(resetCloudCamera);
    }
    if(camera_update)
    {
      camera_update = false;
      viewer.runOnVisualizationThreadOnce(viewNormal);
    }
    if(reset_camera_update)
    {
      reset_camera_update = false;
      viewer.runOnVisualizationThreadOnce(resetCloudCamera);
    }
  }

 	return EXIT_SUCCESS;
}
